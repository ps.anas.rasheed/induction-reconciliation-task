
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>File Comparing</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <!-- Google web fonts -->
    <link href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel='stylesheet'/>
    <!-- The main CSS file -->

    <link rel="stylesheet" href="uploadstyle.css" type="text/css">
    <script>
        $(document).ready(function () {
            $("#sourceNext").on("click", function () {
                $("#sourceDiv").hide();
                $("#targetDiv").show();
            });
            $("#previous").click(function () {
                $("#targetDiv").hide();
                $("#sourceDiv").show();
            });
            $.ajax({
                url: "http://localhost:8080/app/api/availablextensions",
                type: "GET",
                success: function (data) {
                    $(".dropdown").append($("<option     />").val(null).text("Download"));
                    $.each(data, function () {
                        $("#sourceType").append($("<option     />").val(this.toUpperCase()).text(this.toUpperCase()));
                        $("#targetType").append($("<option     />").val(this.toUpperCase()).text(this.toUpperCase()));

                    });
                },
                error: function (err) {

                }
            })
        });
    </script>
</head>
<body>
<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">please insert all information</h3>
                </div>
                <div class="panel-body">
                    <form method="post" enctype="multipart/form-data"
                          action="${pageContext.request.contextPath}/upload" role="form">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div id="sourceDiv">
                                    <input class="form-control" style="margin-top: 7px;" type="text" name="sourceName" placeholder="Source Name"/>
                                    <select class="form-control" style="margin-top: 7px;" class="dropdown mt-1" name="sourceType" id="sourceType"/>

                                    <input  style="margin-top: 7px;" id="sourceFile" type="file" name="file">
                                    <input class="btn-primary" style="margin-top: 7px;" id="sourceNext" type="button" value="Next"/>
                                </div>
                                <div id="targetDiv" style="display:none">
                                    <input class="form-control" style="margin-top: 7px;" id="targetInput" type="text" name="targetName" placeholder="target name"
                                           required/>
                                    <select class="form-control" style="margin-top: 7px;" class="dropdown" id="targetType" name="targetType" required>
                                    </select>
                                    <input  style="margin-top: 7px;" id="targetFile" type="file" name="file" required>
                                    <input class="btn-primary" class="btn-primary" style="margin-top: 7px;" id="previous" type="button" value="previous"/>
                                    <input class="btn-primary" style="margin-top: 7px;" id="targetNext" type="submit" value="Next"/>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <p style="color:#c0392b">${requestScope['uploadFail']}</p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>