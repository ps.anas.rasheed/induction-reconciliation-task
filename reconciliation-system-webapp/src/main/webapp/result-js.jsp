<%@page contentType="text/javascript"%>
    $("#nav-home-tab").on("click", function () {
    $(this).val("matching-transaction");
    $("#nav-profile-tab").val("");
    $("#nav-contact-tab").val("");
});
$("#nav-profile-tab").on("click", function () {
    $(this).val("mismatched-transaction");
    $("#nav-home-tab").val("");
    $("#nav-contact-tab").val("");
});
$("#nav-contact-tab").on("click", function () {
    $(this).val("missing-transaction");
    $("#nav-home-tab").val("");
    $("#nav-profile-tab").val("");
});
$("#nav-home-tab").on("click", function () {
    $.ajax({
        url: "${pageContext.request.contextPath}/api/matchresult",
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            var data = $.parseJSON(result);
            var headers = "<tr><th>#</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>"
            $("#nav-home thead").html(headers)
            $.each(data, function (i, record) {
                var selector = $("#nav-home tbody");
                if (i === 0) {
                    selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + record.transId + "</td>"
                        + "<td>" + record.amount + "</td>" + "<td>" + record.currencyCode + "</td>" + "<td>" + record.date + "</td>" + "</tr>")
                } else
                    selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + record.transId + "</td>"
                        + "<td>" + record.amount + "</td>" + "<td>" + record.currencyCode + "</td>" + "<td>" + record.date + "</td>" + "</tr>")
            })
        }
    });
});
$("#nav-profile-tab").on("click", function () {
    $.ajax({
        url: "${pageContext.request.contextPath}/api/mismatchresult",
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            var data = $.parseJSON(result);
            var headers = "<tr> <th>#</th>" + "<th>" + "found in" + "</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>"
            $("#nav-profile thead").html(headers)
            $.each(data, function (i, record) {
                var selector = $("#nav-profile tbody");
                if (i === 0) {
                    selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + record.foundIn + "</td>" + "</td>" + "<td>" + record.transId + "</td>"
                        + "<td>" + record.amount + "</td>" + "<td>" + record.currencyCode + "</td>" + "<td>" + record.date + "</td>" + "</tr>")
                } else
                    selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + record.foundIn + "</td>" + "<td>" + record.transId + "</td>"
                        + "<td>" + record.amount + "</td>" + "<td>" + record.currencyCode + "</td>" + "<td>" + record.date + "</td>" + "</tr>")
            })
        }
    });
});
$("#nav-contact-tab").on("click", function () {
    $.ajax({
        url: "${pageContext.request.contextPath}/api/missingresult",
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            var data = $.parseJSON(result);
            var headers = "<tr><th>#</th>" + "<th>" + "found in" + "</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>"
            $("#nav-contact thead").html(headers)
            $.each(data, function (i, record) {
                var selector = $("#nav-contact tbody");
                if (i === 0) {
                    selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "</td>" + "<td>" + record.foundIn + "</td>" + "<td>" + record.transId + "</td>"
                        + "<td>" + record.amount + "</td>" + "<td>" + record.currencyCode + "</td>" + "<td>" + record.date + "</td>" + "</tr>")
                } else
                    selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "</td>" + "<td>" + record.foundIn + "</td>" + "</td>" + "<td>" + record.transId + "</td>"
                        + "<td>" + record.amount + "</td>" + "<td>" + record.currencyCode + "</td>" + "<td>" + record.date + "</td>" + "</tr>")
            })
        }
    });
});




var matchedHref;
$(document).ready(function () {
    $.ajax({
        url: "${pageContext.request.contextPath}/api/matchresult",
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            $("#nav-home-tab").val("matching-transaction");
            $("#nav-profile-tab").val("");
            $("#nav-contact-tab").val("");
            var data = $.parseJSON(result);
            var headers = "<tr><th>#</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>";
            $("#nav-home thead").html(headers);
            $.each(data, function (i, record) {
                var selector = $("#nav-home tbody");
                if (i === 0) {
                    selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + record.transId + "</td>"
                        + "<td>" + record.amount + "</td>" + "<td>" + record.currencyCode + "</td>" + "<td>" + record.date + "</td>" + "</tr>")
                } else
                    selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + record.transId + "</td>"
                        + "<td>" + record.amount + "</td>" + "<td>" + record.currencyCode + "</td>" + "<td>" + record.date + "</td>" + "</tr>")
            })
        }
    });
});
var oldCsvHref = $("#csvbtn").attr("href");
$("#csvbtn").on("click", function () {
    if ($("#nav-home-tab").val().length > 1) {
        $(this).attr("href", oldCsvHref);
        $("#csvbtn").attr("href", $("#csvbtn").attr("href") + "/" + $("#nav-home-tab").val() + ".csv");
    }
    if ($("#nav-profile-tab").val().length > 1) {
        $(this).attr("href", oldCsvHref);
        $(this).attr("href", $(this).attr("href") + "/" + $("#nav-profile-tab").val() + ".csv");
    }
    if ($("#nav-contact-tab").val().length > 1) {
        $(this).attr("href", oldCsvHref);
        $(this).attr("href", $(this).attr("href") + "/" + $("#nav-contact-tab").val() + ".csv");
    }
});
var oldJsonBtn = $("#jsonbtn").attr("href");
$("#jsonbtn").on("click", function () {
    if ($("#nav-home-tab").val().length > 1) {
        $(this).attr("href", oldJsonBtn);
        $("#jsonbtn").attr("href", $("#jsonbtn").attr("href") + "/" + $("#nav-home-tab").val() + ".json");
    }
    if ($("#nav-profile-tab").val().length > 1) {
        $(this).attr("href", oldJsonBtn);
        $(this).attr("href", $(this).attr("href") + "/" + $("#nav-profile-tab").val() + ".json");
    }
    if ($("#nav-contact-tab").val().length > 1) {
        $(this).attr("href", oldJsonBtn);
        $(this).attr("href", $(this).attr("href") + "/" + $("#nav-contact-tab").val() + ".json");
    }
});