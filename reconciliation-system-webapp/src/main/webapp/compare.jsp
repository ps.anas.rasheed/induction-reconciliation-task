<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<div class="container mt-5">
<form action="${pageContext.request.contextPath}/compare" method="post">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <h5 class="card-header">Source</h5>
                <div class="card-body">
                    <p>Name: ${sessionScope['fileProperty'].sourceName}</p>
                    <p>Name: ${sessionScope['fileProperty'].sourceExtension}</p>
                </div>
                </div>
            </div>

        <div class="col-md-6">
            <div class="card">
                <h5 class="card-header">Target</h5>
                <div class="card-body">
                    <p>Name: ${sessionScope['fileProperty'].targetName}</p>
                    <p>Name: ${sessionScope['fileProperty'].targetExtension}</p>
                </div>
            </div>
    </div>
    <div class="row p-5">
        <input type="submit" value="Compare" class="btn btn-primary"/>
        &nbsp;
        <a href="${pageContext.request.contextPath}/upload">
            <input type="button" value="Cancel" class="btn btn-primary"/>
        </a>
    </div>
</form>
</div>
</body>
</html>

