package org.progressoft.helpers;

import org.progressoft.engine.Record;
import org.progressoft.utils.Consumer;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AllRecordsConsumer implements Consumer {


    private Map<String, Record> records = new HashMap<>();
    private boolean finish = false;

    @Override
    public boolean isEnd() {
        return finish;
    }

    @Override
    public void accept(Record record) {
        if (!Objects.isNull(record)) {
            String transId = record.getTransId();
            if (records.get(record.getTransId()) != null)
                transId = record.getTransId() + "mis";

            // throw new IllegalStateException("the transaction id : " + record.getTransId() + " is duplicate in csv file, please correct the file");
            records.put(transId, record);
        }
    }

    public Map<String, Record> getRecords() {
        return new HashMap<>(records);
    }

    public boolean isFinish() {
        return finish;
    }
}