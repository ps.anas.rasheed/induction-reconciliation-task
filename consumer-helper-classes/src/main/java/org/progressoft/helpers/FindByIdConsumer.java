package org.progressoft.helpers;

import org.progressoft.engine.Record;
import org.progressoft.utils.Consumer;

public class FindByIdConsumer implements Consumer {
    String transId;
    boolean found = false;

    public FindByIdConsumer(String code) {
        this.transId = code;
    }

    @Override
    public void accept(Record record) {
        found = (record.getTransId().equalsIgnoreCase(transId));
    }

    @Override
    public boolean isEnd() {
        return found;
    }

    public String getTransId() {
        return new String(transId);
    }

    public boolean isFound() {
        return found;
    }
}
