package orj.progressoft.reconciliation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.progressoft.Engine.CSVDataSource;
import org.progressoft.Engine.CSVRecordService;
import org.progressoft.Engine.JsonDataSource;
import org.progressoft.engine.Record;
import org.progressoft.engine.SimpleFilesComparing;
import org.progressoft.exceptions.FileExcptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class CompareTest {

    // TODO paths are absolute
    public static final String expectedPath = "/home/u822/jip8/gitlab/anas/sample-files/.";
    public static final String csvPath = "./input-files/bank_trans.csv";
    public static final String invalidCsvPath = "./input-files/invalid_bank_trans.csv";
    public static final String jsonPath = "./input-files/online_bank_trans.json";
    public static final String actualMatchingPath = "./result-files/matching-transactions.csv";
    public static final String actualMissingMatchingPath = "./result-files/missing-transactions.csv";

    @Test
    public void givenInValidPath_whenConstructingCompare_thenThrowExcptions() {
        Assertions.assertThrows(NullPointerException.class, () -> new SimpleFilesComparing().compare(new CSVDataSource(Paths.get(null)), new JsonDataSource(Paths.get(null))));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new SimpleFilesComparing().compare(new CSVDataSource(Paths.get(".")), new JsonDataSource(Paths.get("."))));
        Assertions.assertThrows(FileExcptions.class, () -> new SimpleFilesComparing().compare(new CSVDataSource(Paths.get(jsonPath)), new JsonDataSource(Paths.get(csvPath))));
//        Assertions.assertThrows(ParsingException.class, () -> new SimpleFilesComparing().compare(new CSVDataSource(Paths.get(invalidCsvPath)), new JsonDataSource(Paths.get(jsonPath))));
    }

    @Test
    public void givenValidPath_whenComparing_thenRetuenResult() {
//        FileComparing comparing = new SimpleFilesComparing(new CSVDataSource(Paths.get(csvPath)), new JsonDataSource(Paths.get(jsonPath)));
//        Assertions.assertEquals(expectedPath, comparing.compare());
//        comparing = new SimpleFilesComparing(new JsonDataSource(Paths.get(jsonPath)), new CSVDataSource(Paths.get(csvPath)));
//        Assertions.assertEquals(expectedPath, comparing.compare());
//        comparing = new SimpleFilesComparing(new JsonDataSource(Paths.get(jsonPath)), new JsonDataSource(Paths.get(jsonPath)));
//        Assertions.assertEquals(expectedPath, comparing.compare());
//        comparing = new SimpleFilesComparing(new CSVDataSource(Paths.get(csvPath)), new CSVDataSource(Paths.get(csvPath)));
//        Assertions.assertEquals(expectedPath, comparing.compare());

    }


    private void findResult(String resultpath, String actualTypeFile) {
        CSVRecordService result = new CSVRecordService(Paths.get(resultpath));
        Map<String, Record> resultRecords = result.getRemainingRecords();
        CSVRecordService source = new CSVRecordService(Paths.get(actualTypeFile));
        Map<String, Record> actualRecords = source.getRemainingRecords();
        Assertions.assertEquals(resultRecords.size(), actualRecords.size());
        actualRecords.forEach((s, record) ->
                {
                    Record resultRec = resultRecords.get(record.getTransId());
                    Assertions.assertTrue(resultRec.equals(record));
                }
        );
    }


    @Test
    public void givenValidPath_whenComparingMisMatchedTransaction_thenReturnMatchingRecordsBetweenInputAndOutputFiles() {
        String resultpath = "/home/u822/jip8/gitlab/anas/mismatched-transaction.csv";

        try (BufferedReader reader = Files.newBufferedReader(Paths.get(resultpath))) {
            String header = reader.readLine();
            String record1 = reader.readLine();
            String record2 = reader.readLine();
            String record3 = reader.readLine();
            String record4 = reader.readLine();
            String actualHeader = "found in file,transaction id,amount,currecny code,value date";
            String actualRecord1 = "SOURCE,TR-47884222202,20.000,JOD,2020-01-22";
            String actualRecord2 = "TARGET,TR-47884222202,30.000,JOD,2020-01-22";
            String actualRecord3 = "SOURCE,TR-47884222205,60.000,JOD,2020-02-02";
            String actualRecord4 = "TARGET,TR-47884222205,60.000,JOD,2020-02-03";

            Assertions.assertEquals(actualHeader, header);
            Assertions.assertEquals(actualRecord1, record1);
            Assertions.assertEquals(actualRecord2, record2);
            Assertions.assertEquals(actualRecord3, record3);
            Assertions.assertEquals(actualRecord4, record4);

        } catch (IOException e) {
            Assertions.fail(e);
        }
    }
}