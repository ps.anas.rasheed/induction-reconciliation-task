package org.progressoft.system;

import org.apache.commons.io.FilenameUtils;

import java.nio.file.Files;
import java.nio.file.Path;

// TODO rename this to another thing DONE
public class PathHelper {
    private Path path;
    private String extension;

    public PathHelper(Path path, String extension) {
        validateFiles(path, extension);
        this.path = path;
        this.extension = extension;
    }

    public Path getPath() {
        return path;
    }

    public String getExtension() {
        return extension.trim();
    }

    private void validateFiles(Path path, String extension) {
        if (!extension.equalsIgnoreCase(FilenameUtils.getExtension(path.toString())))
            throw new IllegalStateException("the file path must end with " + "the same extension that you entered " + extension);
//        if (!Files.exists(path))
//            throw new NullPointerException("the directory is InCorrect : " + path.toString());
    }
}
