package org.progressoft.system;

import org.progressoft.utils.FileComparing;
import org.progressoft.utils.FilesDataSource;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.ServiceLoader;

public class App {

    /*
    /home/user/Evaluation Task/induction-reconciliation-task/sample-files/input-files/bank_trans.csv
    csv
    /home/user/Evaluation Task/induction-reconciliation-task/sample-files/input-files/online_bank_trans.json
    json

    dynamic proxy  (invoke (object , method , args))
    service loader call all implementation

         */
    public static void main(String[] args) {

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println(">> Enter source file location: ");
            Path source = Paths.get(scanner.nextLine().trim());
            System.out.println(">> Enter source file format: ");
            String extensionSource = scanner.nextLine().trim();
            System.out.println(">> Enter target file location: ");
            Path target = Paths.get(scanner.nextLine().trim());
            System.out.println(">> Enter target file format: ");
            String extensionTarget = scanner.nextLine().trim();

            PathHelper sourceObj = new PathHelper(source, extensionSource);
            PathHelper targetObj = new PathHelper(target, extensionTarget);

            ServiceLoader<FileComparing> serviceLoader = ServiceLoader.load(FileComparing.class);
            for (FileComparing fileComparing : serviceLoader) {
                System.out.println("");
                fileComparing.compare(setupDataSource(sourceObj), setupDataSource(targetObj));
            }
        }
    }

    private static FilesDataSource setupDataSource(PathHelper targetPath) {
        return FilesDataSourceFactory.getSourceExtension(targetPath);
    }
    // TODO enhance the design by moving this method to another class, name FilesDataSourceFactory DONE
}
