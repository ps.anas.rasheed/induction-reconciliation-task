package org.progressoft.system;

import org.progressoft.Engine.CSVDataSource;
import org.progressoft.Engine.JsonDataSource;
import org.progressoft.utils.FilesDataSource;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class FilesDataSourceFactory {
    private static HashMap<String, Factory> factories = new HashMap<>();

    static {
        factories.put("csv", FilesDataSourceFactory::newCSVDataSource);
        factories.put("json", FilesDataSourceFactory::newJSONDataSource);
    }

    public static FilesDataSource getSourceExtension(PathHelper path) {
        //null object factory
        Factory factory = factories.getOrDefault(path.getExtension().toLowerCase(), (e) -> {
            throw new IllegalArgumentException("Unknown file type: " + path.getExtension());
        });

        return factory.newDataSource(path.getPath());
    }

    public static List<String> getAvailableExtensions() {
        Set<String> extensions = factories.keySet();
        return new ArrayList<>(extensions);
    }


    private static CSVDataSource newCSVDataSource(Path path) {
        return new CSVDataSource(path);
    }

    private static JsonDataSource newJSONDataSource(Path path) {
        return new JsonDataSource(path);
    }

    interface Factory {
        FilesDataSource newDataSource(Path path);
    }
}
