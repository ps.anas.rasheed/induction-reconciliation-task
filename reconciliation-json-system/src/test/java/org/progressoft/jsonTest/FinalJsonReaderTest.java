package org.progressoft.jsonTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.progressoft.Engine.FinalJsonReader;
import org.progressoft.Engine.JsonRecordServices;
import org.progressoft.engine.Record;
import org.progressoft.exceptions.FileExcptions;
import org.progressoft.helpers.AllRecordsConsumer;
import org.progressoft.utils.RecordServices;

import javax.json.stream.JsonParsingException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class FinalJsonReaderTest extends BaseTest {

    @Test
    public void givenNullPath_whenConstructing_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> new FinalJsonReader(null));
        Assertions.assertEquals("null path", exception.getMessage());
    }


    @Test
    public void givenNonExistPath_whenConstructing_thenThrowFileNotFoundException() {
        Path path = Paths.get("test/path");
        FileExcptions fileNotFoundException = Assertions.assertThrows(FileExcptions.class,
                () -> new FinalJsonReader(path));
        Assertions.assertEquals("file not found in path : " + path.toAbsolutePath().toString(), fileNotFoundException.getMessage());

    }

    @Test
    public void givenInvalidFile_whenReading_thenThrowParsingFileException() {
        Path path = Paths.get("../sample-files/input-files/invalid_bank_trans.csv");
        JsonParsingException fileExcptions = Assertions.assertThrows(JsonParsingException.class,
                () -> new FinalJsonReader(path).read(new AllRecordsConsumer()));

    }
//
//    @Test
//    public void givenValidFile_whenReading_thenReturnTrueResult() {
//        Path path=Paths.get("../matching-transaction.json");
//        AllRecords allRecords = new AllRecords();
//        FinalJsonReader reader = new FinalJsonReader(path);
//        reader.read(allRecords);
//        Assertions.assertEquals(3, allRecords.getRecords().size());
//    }

    @Test
    public void givenValidRecord_whenExecuteJSONProvider_ReturnTheResultIsExist() throws IOException, ParseException {

        String testCSVRecord = "TR-47884222201,140.0,USD,2020-01-20";
        String[] testRecord = testCSVRecord.split(",");
        testRecord[testRecord.length - 1] = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").parse(testRecord[testRecord.length - 1]));
        RecordServices recordServices = new JsonRecordServices(BaseTest.data);
        Record record = recordServices.findRecord("TR-47884222201");
        assertArrayEquals(testRecord, record.toArray());
    }

}
