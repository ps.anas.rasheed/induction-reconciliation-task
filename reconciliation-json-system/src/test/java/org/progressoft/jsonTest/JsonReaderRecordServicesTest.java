package org.progressoft.jsonTest;

import org.junit.jupiter.api.Test;
import org.progressoft.Engine.JsonRecordServices;
import org.progressoft.engine.Record;
import org.progressoft.exceptions.FileExcptions;
import org.progressoft.utils.RecordServices;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

public class JsonReaderRecordServicesTest extends BaseTest {

    @Test
    public void givenInvalidPath_whenConstructingCSVReaderProvider_thenFail() {
        NullPointerException nullPointerException = assertThrows(NullPointerException.class, () -> new JsonRecordServices(null));
        assertEquals("null path", nullPointerException.getMessage());

        Path notExistPath = Paths.get(".", "Abc.txt");
        assertThrows(FileExcptions.class, () -> new JsonRecordServices(notExistPath));

        Path dir = Paths.get(".");
        assertThrows(IllegalArgumentException.class, () -> new JsonRecordServices(dir));
    }

    @Test
    public void givenValidRecord_whenExecuteJSONprovider_ReturnTheResultIsExist() throws IOException, ParseException {

        String testCSVRecord = "TR-47884222201,140.0,USD,2020-01-20";
        String[] testRecord = testCSVRecord.split(",");
        testRecord[testRecord.length - 1] = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").parse(testRecord[testRecord.length - 1]));
        RecordServices recordServices = new JsonRecordServices(BaseTest.data);
        Record record = recordServices.findRecord("TR-47884222201");
        assertArrayEquals(testRecord, record.toArray());
    }
}
