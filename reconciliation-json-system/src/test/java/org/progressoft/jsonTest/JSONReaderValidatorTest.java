package org.progressoft.jsonTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.progressoft.Engine.JsonRecordServices;
import org.progressoft.exceptions.FileExcptions;
import org.progressoft.exceptions.ParsingException;
import org.progressoft.utils.RecordServices;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JSONReaderValidatorTest extends BaseTest {

    // TODO use relative paths
    public static final String invalidJsonPath = "/home/u822/jip8/gitlab/anas/sample-files/input-files/invalid_online_bank_trans.json";

    @Test
    public void givenNullPath_whenConstructing_thenThrowNullPointerException() {
        NullPointerException nullPathException = Assertions.assertThrows(NullPointerException.class
                , () -> new JsonRecordServices(null));
        Assertions.assertEquals("null path", nullPathException.getMessage());
    }

    @Test
    public void givenNonExistPath_whenConstructing_thenThrowFileNotFoundException() {
        Path path = Paths.get("test/path");
        FileExcptions fileNotFoundException = Assertions.assertThrows(FileExcptions.class,
                () -> new JsonRecordServices(path));
        Assertions.assertEquals("file not found in path : " + path.toAbsolutePath().toString(), fileNotFoundException.getMessage());

    }

    @Test
    public void givenDirectory_whenConstructing_thenThrowIllegalArgumentException() {
        Path path = Paths.get(".");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new JsonRecordServices(path));
        Assertions.assertEquals("Not file in path " + path.toAbsolutePath().toString(), exception.getMessage());
    }

    @Test
    public void givenNullTransId_whenValidating_thenThrowNullPointerException() throws FileNotFoundException {
        RecordServices validator = new JsonRecordServices(data);
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> validator.contains(null));
        Assertions.assertEquals("Null Trans_id", nullPointerException.getMessage());
    }

    @Test
    public void givenTransId_whenIsValidate_thenReturnExpectedResult() throws FileNotFoundException {
        RecordServices validator = new JsonRecordServices(data);
        boolean code = validator.contains("TR-47884222201");
        Assertions.assertTrue(code);
        Assertions.assertFalse(validator.contains("test"));
    }

    @Test
    public void givenInvalidFile_whenExecuteCSVServices_thenThrowParsingEception() {
        Assertions.assertThrows(ParsingException.class, () -> new JsonRecordServices(Paths.get(invalidJsonPath)));
    }

}
