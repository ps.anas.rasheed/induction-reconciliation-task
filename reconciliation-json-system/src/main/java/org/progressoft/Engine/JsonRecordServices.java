package org.progressoft.Engine;

import org.progressoft.engine.Record;
import org.progressoft.helpers.AllRecordsConsumer;
import org.progressoft.helpers.FindByIdConsumer;
import org.progressoft.utils.RecordServices;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class JsonRecordServices implements RecordServices {
    private   JSONReader jsonReader;
    private  Map<String, Record> map;

    public JsonRecordServices(Path JsonPath) {
        jsonReader = new JSONReader(JsonPath);
        map = new HashMap<>();
        map = readAllRecords();
    }

    private Map<String, Record> readAllRecords() {
        AllRecordsConsumer allRecordsConsumer = new AllRecordsConsumer();
        jsonReader.read(allRecordsConsumer);
        return allRecordsConsumer.getRecords();

    }

    @Override
    public Record findRecord(String transId) {
        nullValidation(transId);
        Record record = map.get(transId);
        if (Objects.isNull(record))
            throw new NullPointerException("the value of " + transId + " is not found in Json file");

        return record;

    }

    @Override
    public Map<String, Record> getRemainingRecords() {
        return new HashMap<>(map);

    }

    @Override
    public boolean contains(String transId) {
        nullValidation(transId);
        FindByIdConsumer findByIdConsumer = new FindByIdConsumer(transId);
        jsonReader.read(findByIdConsumer);
        if (!findByIdConsumer.isFound())
            return false;
        return true;
    }

    @Override
    public Record ignoreRecord(String transId) {
        nullValidation(transId);
        Record record = map.remove(transId);
        if (Objects.isNull(record))
            throw new NullPointerException("the value of " + transId + " is not found in Json file");

        return record;
    }

    private void nullValidation(String transId) {
        if (Objects.isNull(transId))
            throw new NullPointerException("Null Trans_id");
    }

    // TODO those are duplicates in CSVRecordService DONE

}
