package org.progressoft.Engine;

import org.progressoft.utils.FilesDataSource;
import org.progressoft.utils.RecordServices;

import java.nio.file.Path;

public class JsonDataSource implements FilesDataSource {

    JsonRecordServices provider;

    public JsonDataSource(Path path) {
        provider = new JsonRecordServices(path);
    }


    @Override
    public RecordServices getServices() {
        return provider;
    }


}
