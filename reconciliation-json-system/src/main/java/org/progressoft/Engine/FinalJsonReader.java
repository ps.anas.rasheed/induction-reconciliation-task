package org.progressoft.Engine;

import org.progressoft.engine.Record;
import org.progressoft.exceptions.FileExcptions;
import org.progressoft.exceptions.ParsingException;
import org.progressoft.utils.Consumer;
import org.progressoft.utils.Reader;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Currency;
import java.util.Date;
import java.util.Objects;

public class FinalJsonReader implements Reader {

    private  File file;

    public static final String DATE = "date";
    public static final String REFERENCE = "transId";
    public static final String AMOUNT = "amount";
    public static final String CURRENCYCODE = "currencyCode";

    public FinalJsonReader(Path path) {
        isValidPath(path);
        file = new File(path.toString());
    }

    @Override
    public void read(Consumer consumer) {
        try (InputStream inputStream = new FileInputStream(file);
             JsonReader jsonReader = Json.createReader(inputStream)) {
            JsonArray AllRecords = jsonReader.readArray();
            handleRecords(consumer, AllRecords);
        } catch (IOException e) {
            throw new FileExcptions("can not read the file ", e);
        }
    }

    private void handleRecords(Consumer consumer, JsonArray allRecords) {
        Record record;
        for (int i = 0; i < allRecords.size(); i++) {
            JsonObject jsonObject = (JsonObject) allRecords.get(i);
            record = getRecords(jsonObject);
            consumer.accept(record);
            if (consumer.isEnd())
                break;
        }
    }

    private Record getRecords(JsonObject object) {
        try {
            String[] rec = getRecord(object);
            String transIs = rec[1].trim();
            String currency = rec[3].trim();
            int scale = getCurrencyFractionsDigits(currency);
            BigDecimal amount = new BigDecimal(rec[2].trim()).setScale(scale);
            return new Record(transIs, amount, currency, new Date(rec[0].trim()));

        } catch (NumberFormatException exc) {
            throw new ParsingException("can not parse , please correct file format ", exc);
        }

    }

    private int getCurrencyFractionsDigits(String currency) {
        try {
            Currency currencyObj = Currency.getInstance(currency);
            return currencyObj.getDefaultFractionDigits();

        } catch (IllegalArgumentException exc) {
            throw new ParsingException("the currency" + currency + " is not following ISO 4217 alphabetic code", exc);
        }
    }


    private String[] getRecord(JsonObject jsonObject) {
        return new String[]{
                jsonObject.getString(DATE),
                jsonObject.getString(REFERENCE),
                jsonObject.getJsonNumber(AMOUNT).bigDecimalValue().toString(),
                jsonObject.getString(CURRENCYCODE),
        };
    }

    private void isValidPath(Path path) {
        Objects.requireNonNull(path, "null path");
        if (Files.notExists(path))
            throw new FileExcptions("file not found in path : " + path.toAbsolutePath().toString());
    }
}
