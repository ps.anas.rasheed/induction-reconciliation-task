package org.progressoft.csvTest;

import org.junit.jupiter.api.BeforeAll;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class BaseTest {
    public static Path data;

    @BeforeAll
    public static void copySampleFile() throws IOException {
        try (InputStream is = CSVReaderValidatorTest.class.getResourceAsStream("/bank_trans.csv")) {
            data = Files.createTempFile("transData", ".csv");
            try (OutputStream os = Files.newOutputStream(data)) {
                int length;
                byte[] buffer = new byte[1024 * 8];
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
                os.flush();
            }
        }
    }
}

