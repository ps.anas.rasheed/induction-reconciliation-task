package org.progressoft.csvTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.progressoft.Engine.CSVRecordService;
import org.progressoft.engine.Record;
import org.progressoft.exceptions.FileExcptions;
import org.progressoft.exceptions.ParsingException;
import org.progressoft.utils.RecordServices;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

public class CSVReaderRecordServicesTest extends BaseTest {

    // TODO use relative paths instead
    public static final String invalidCsvPath = System.getProperty("user.home") + "/jip8/gitlab/anas/sample-files/input-files/invalid_bank_trans.csv";

    @Test
    public void givenInvalidPath_whenConstructingCSVReaderProvider_thenFail() {
        NullPointerException nullPointerException = assertThrows(NullPointerException.class, () -> new CSVRecordService(null));
        assertEquals(null, nullPointerException.getMessage());

        Path notExistPath = Paths.get(".", "Abc.txt");
        assertThrows(FileExcptions.class, () -> new CSVRecordService(notExistPath));

        Path dir = Paths.get(".");
        assertThrows(IllegalArgumentException.class, () -> new CSVRecordService(dir));
    }

    @Test
    public void givenValidRecord_whenExecuteCSVProvider_ReturnTheResultIsExist() throws IOException, ParseException {

        String testCSVRecord = "TR-47884222201,140.0,USD,2020-01-20";
        String[] testRecord = testCSVRecord.split(",");
        testRecord[testRecord.length - 1] = String.valueOf(new SimpleDateFormat("yyyy-MM-dd").parse(testRecord[testRecord.length - 1]));
        RecordServices recordServices = new CSVRecordService(BaseTest.data);
        Record record = recordServices.findRecord("TR-47884222201");
        assertArrayEquals(testRecord, record.toArray());
    }

    @Test
    public void givenInvalidFile_whenExecuteCSVServices_thenThrowParsingEception() {
        Assertions.assertThrows(ParsingException.class, () -> new CSVRecordService(Paths.get(invalidCsvPath)));
    }

}
