package org.progressoft.csvTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.progressoft.Engine.CSVRecordService;
import org.progressoft.exceptions.FileExcptions;
import org.progressoft.utils.RecordServices;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CSVReaderValidatorTest extends BaseTest {

    @Test
    public void givenNullPath_whenConstructing_thenThrowNullPointerException() {
        NullPointerException nullPathException = Assertions.assertThrows(NullPointerException.class
                , () -> new CSVRecordService(null));
        Assertions.assertEquals(null, nullPathException.getMessage());
    }

    @Test
    public void givenNonExistPath_whenConstructing_thenThrowFileNotFoundException() {
        Path path = Paths.get("test/path");
        FileExcptions fileNotFoundException = Assertions.assertThrows(FileExcptions.class,
                () -> new CSVRecordService(path));
        Assertions.assertEquals("file not found in path : " + path.toAbsolutePath().toString(), fileNotFoundException.getMessage());

    }

    @Test
    public void givenDirectory_whenConstructing_thenThrowIllegalArgumentException() {
        Path path = Paths.get(".");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new CSVRecordService(path));
        Assertions.assertEquals("Not file", exception.getMessage());
    }

    @Test
    public void givenNullTransId_whenValidating_thenThrowNullPointerException() throws FileNotFoundException {
        RecordServices validator = new CSVRecordService(data);
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> validator.contains(null));
        Assertions.assertEquals("Null Trans_id", nullPointerException.getMessage());
    }

    @Test
    public void givenTransId_whenIsValidate_thenReturnExpectedResult() throws FileNotFoundException {
        RecordServices validator = new CSVRecordService(data);
        boolean code = validator.contains("TR-47884222204");
        Assertions.assertTrue(code);
        Assertions.assertFalse(validator.contains("test"));

    }
}

