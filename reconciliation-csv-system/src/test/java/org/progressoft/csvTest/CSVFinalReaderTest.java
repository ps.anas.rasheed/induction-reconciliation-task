package org.progressoft.csvTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.progressoft.Engine.FinalCsvReader;
import org.progressoft.exceptions.FileExcptions;
import org.progressoft.exceptions.ParsingException;
import org.progressoft.helpers.AllRecordsConsumer;

import java.nio.file.Path;
import java.nio.file.Paths;

public class CSVFinalReaderTest extends BaseTest {


    @Test
    public void givenNullPath_whenConstructing_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> new FinalCsvReader(null));
        Assertions.assertEquals("null", exception.getMessage());
    }


    @Test
    public void givenNonExistPath_whenConstructing_thenThrowFileNotFoundException() {
        Path path = Paths.get("test/path");
        FileExcptions fileNotFoundException = Assertions.assertThrows(FileExcptions.class,
                () -> new FinalCsvReader(path));
        Assertions.assertEquals("file not found in path : " + path.toAbsolutePath().toString(), fileNotFoundException.getMessage());

    }

    @Test
    public void givenInvalidFile_whenReading_thenThrowParsingFileException() {
        Path path = Paths.get("../sample-files/input-files/invalid_bank_trans.csv");
        ParsingException fileExcptions = Assertions.assertThrows(ParsingException.class,
                () -> new FinalCsvReader(path).read(new AllRecordsConsumer()));
        Assertions.assertEquals("can not parse , please correct file format".trim(), fileExcptions.getMessage().trim());


    }

    @Test
    public void givenValidFile_whenReading_thenReturnTrueResult() {
        AllRecordsConsumer allRecordsConsumer = new AllRecordsConsumer();
        FinalCsvReader reader = new FinalCsvReader(data);
        reader.read(allRecordsConsumer);
        Assertions.assertEquals(6, allRecordsConsumer.getRecords().size());
    }
}
