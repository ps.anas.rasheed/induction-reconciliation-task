package org.progressoft.Engine;

import org.progressoft.utils.FilesDataSource;
import org.progressoft.utils.RecordServices;

import java.nio.file.Path;

public class CSVDataSource implements FilesDataSource {

     CSVRecordService csvProvider;

    public CSVDataSource(Path path) {
        csvProvider = new CSVRecordService(path);
    }

    @Override
    public RecordServices getServices() {
        return csvProvider;
    }


}
