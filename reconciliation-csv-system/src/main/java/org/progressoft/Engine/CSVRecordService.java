package org.progressoft.Engine;

import org.progressoft.engine.Record;
import org.progressoft.exceptions.NoSuchRecordException;
import org.progressoft.helpers.AllRecordsConsumer;
import org.progressoft.helpers.FindByIdConsumer;
import org.progressoft.utils.RecordServices;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class CSVRecordService implements RecordServices {
    private CSVReader csvReader;
    private Map<String, Record> map;

    public CSVRecordService(Path csvPAth) {
        csvReader = new CSVReader(csvPAth);
        map = new HashMap<>();
        map = readAllRecords();
    }

    @Override
    public Record findRecord(String transId) {
        nullValidation(transId);
        Record record = map.get(transId);
        validateRecord(transId, record);
        return record;

    }

    private void validateRecord(String transId, Record record) {
        if (Objects.isNull(record))
            // TODO NoSuchRecordException DONE
            throw new NoSuchRecordException( "the value of " + transId + " is not found in CSV file");
    }

    private Map<String, Record> readAllRecords() {
        AllRecordsConsumer allRecordsConsumer = new AllRecordsConsumer();
        csvReader.read(allRecordsConsumer);
        return allRecordsConsumer.getRecords();

    }

    @Override
    public Map<String, Record> getRemainingRecords() {
        return new HashMap<>(map);
    }

    @Override
    public boolean contains(String transId) {
        nullValidation(transId);

        FindByIdConsumer FindByIdConsumer = new FindByIdConsumer(transId);
        csvReader.read(FindByIdConsumer);
        if (!FindByIdConsumer.isFound())
            return false;
        return true;

    }

    @Override
    public Record ignoreRecord(String transId) {
        nullValidation(transId);
        Record record = map.remove(transId);
        validateRecord(transId, record);
        return record;
    }

    private void nullValidation(String transId) {
        if (Objects.isNull(transId))
            throw new NullPointerException("Null Trans_id");
    }
}
