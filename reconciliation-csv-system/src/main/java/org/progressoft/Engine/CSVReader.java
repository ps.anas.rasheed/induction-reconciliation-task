package org.progressoft.Engine;

import org.apache.commons.io.FilenameUtils;
import org.progressoft.engine.Record;
import org.progressoft.exceptions.FileExcptions;
import org.progressoft.exceptions.ParsingException;
import org.progressoft.utils.Consumer;
import org.progressoft.utils.Reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Objects;

public class CSVReader implements Reader {

    private Path csvPAth;
    private static int ID;
    private static int AMOUNT;
    private static int CURRENCY;
    private static int DATE;
    public static int numOfColumns;

    public CSVReader(Path path) {
        isValidPath(path);
        csvPAth = path;
    }

    @Override
    public void read(Consumer consumer) {

        try (BufferedReader reader = Files.newBufferedReader(csvPAth)) {
            skipHeader(reader);
            handlingRecords(consumer, reader);
        } catch (IOException exc) {
            throw new FileExcptions("can not read the file ", exc);
        }
    }

    private void handlingRecords(Consumer consumer, BufferedReader reader) {
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                Record record = returnRecord(line);
                consumer.accept(record);
                if (consumer.isEnd())
                    break;
            }
        } catch (IOException e) {
            throw new FileExcptions("can not read the file ", e);
        }
    }

    private void skipHeader(BufferedReader reader) {
        try {
            String header = reader.readLine();
            defineHeaderColumns(header);
            numOfColumns = header.split(",").length;
        } catch (IOException e) {
            throw new FileExcptions("can not read the file ", e);
        }
    }

    private void defineHeaderColumns(String header) {
        String[] columns = header.split(",");
        for (int columNumber = 0; columNumber < columns.length; columNumber++) {
            if (columns[columNumber].trim().equalsIgnoreCase("trans unique id") || columns[columNumber].trim().equalsIgnoreCase("transaction id") || columns[columNumber].trim().equalsIgnoreCase("transId"))
                ID = columNumber;
            else if (columns[columNumber].trim().equalsIgnoreCase("amount"))
                AMOUNT = columNumber;
            else if (columns[columNumber].trim().equalsIgnoreCase("currecny") || columns[columNumber].equalsIgnoreCase("currecny code"))
                CURRENCY = columNumber;
            else if (columns[columNumber].trim().equalsIgnoreCase("value date"))
                DATE = columNumber;
        }

        System.out.println("id "+ID+" amount "+AMOUNT+" currency "+CURRENCY+" date "+DATE);
    }

    private Record returnRecord(String line) {
        String[] record = splitAttributes(line);
        validateNumberOfAttributes(record);
        return returnAttrAsObj(record);
    }

    private Record returnAttrAsObj(String[] record) {
        try {
            String transIs = record[ID].trim();
            String currency = record[CURRENCY].trim();
            int scale = getCurrencyFractionsDigits(currency);
            BigDecimal amount = new BigDecimal(record[AMOUNT].trim()).setScale(scale);
            Date date;
            date = new SimpleDateFormat("yyyy-MM-dd").parse(record[DATE].trim());
            return new Record(transIs, amount, currency, date);
        } catch (ParseException e) {
            throw new ParsingException("can not parse date please check date format", e);
        } catch (NumberFormatException exc) {
            throw new ParsingException("can not parse , please correct file format ", exc);
        }
    }

    private int getCurrencyFractionsDigits(String currency) {
        try {
            Currency currencyObj = Currency.getInstance(currency);
            return currencyObj.getDefaultFractionDigits();

        } catch (IllegalArgumentException exc) {
            throw new ParsingException("the currency " + currency + " is not following ISO 4217 alphabetic code", exc);
        }
    }

    private void validateNumberOfAttributes(String[] record) {
        if (record.length != numOfColumns)
            throw new FileExcptions("the columns for trans id " + record[0] + " is inCorrect");
    }

    private String[] splitAttributes(String l) {
        return l.split(",");
    }

    private void isValidPath(Path path) {
        System.out.println("the path when validate is "+path.toAbsolutePath().toString());
        Objects.requireNonNull(path, "null path");
        if (Files.notExists(path))
            throw new FileExcptions("file not found in path : " + path.toAbsolutePath().toString());
        if (Files.isDirectory(path))
            throw new IllegalArgumentException("Not file");
        if (!FilenameUtils.getExtension(path.toString()).equalsIgnoreCase("csv"))
            throw new FileExcptions("the file path must end with the same extension that you entered (CSV)");
    }
}
