package org.progressoft.reconciliation.web;

import org.progressoft.reconciliation.api.*;
import org.progressoft.reconciliation.servlets.ComparatorServlet;
import org.progressoft.reconciliation.servlets.IndexServlet;
import org.progressoft.reconciliation.servlets.ResultServlet;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;
import java.util.logging.Logger;

public class RecnciliationInitalizer implements ServletContainerInitializer {

    private Logger logger = Logger.getLogger(RecnciliationInitalizer.class.getName());

    @Override
    public void onStartup(Set<Class<?>> set, ServletContext ctx) throws ServletException {
        applyIndexServlet(ctx);
        applyExtensionsApi(ctx);
        applyIResultServlet(ctx);
        // TODO those could be in one servlet
        applyMatchingResultApi(ctx);
        applyMissingResultApi(ctx);
        applyMisMatchingResultApi(ctx);

        // TODO download servlets could be refactored as one single servlet injected with a file generator
        applyDownloadJson(ctx);
        applyDownloadCsv(ctx);

    }

    private void applyDownloadCsv(ServletContext ctx) {
        DownloadCSV downloadCSV = new DownloadCSV();
        ServletRegistration.Dynamic registrationApi = ctx.addServlet("downloadCsv", downloadCSV);
        registrationApi.addMapping("/downloadcsv/*");

    }

    private void applyDownloadJson(ServletContext ctx) {
        DownloadJson downloadJson = new DownloadJson();
        ServletRegistration.Dynamic registrationApi = ctx.addServlet("downloadJson", downloadJson);
        registrationApi.addMapping("/downloadjson/*");
    }

    private void applyMisMatchingResultApi(ServletContext ctx) {
        MisMatchResultApi misMatchResultApi = new MisMatchResultApi();
        ServletRegistration.Dynamic registrationApi = ctx.addServlet("MisMatchingResult", misMatchResultApi);
        registrationApi.addMapping("/api/mismatchresult");
    }

    private void applyMissingResultApi(ServletContext ctx) {
        MissingResultApi missingResultApi = new MissingResultApi();
        ServletRegistration.Dynamic registrationApi = ctx.addServlet("MissingResult", missingResultApi);
        registrationApi.addMapping("/api/missingresult");
    }

    private void applyComparatorServlet(ServletContext ctx) {
        ComparatorServlet servlet = new ComparatorServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("comparatorServlet", servlet);
        registration.addMapping("/compare");
        System.out.println("entered method");
    }

//    private void applyUploadServlet(ServletContext ctx) {
//        UploadServlet uploadServlet = new UploadServlet();
//        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("uploadServlet", uploadServlet);
//        servletRegistration.addMapping("/upload");
//    }

    private void applyIndexServlet(ServletContext ctx) {
        IndexServlet indexServlet = new IndexServlet();
        ServletRegistration.Dynamic registrationIndex = ctx.addServlet("index", indexServlet);
        registrationIndex.addMapping("/index");
    }

    private void applyIResultServlet(ServletContext ctx) {
        ResultServlet resultServlet = new ResultServlet();
        ServletRegistration.Dynamic registrationIndex = ctx.addServlet("result", resultServlet);
        registrationIndex.addMapping("/result");
    }

    private void applyExtensionsApi(ServletContext ctx) {
        ExtensionApi extensionApi = new ExtensionApi();
        ServletRegistration.Dynamic registrationApi = ctx.addServlet("ExtensionsApi", extensionApi);
        registrationApi.addMapping("/api/availablextensions");
    }

    private void applyMatchingResultApi(ServletContext ctx) {
        MatchingResultApi matchingResultApi = new MatchingResultApi();
        ServletRegistration.Dynamic registrationApi = ctx.addServlet("MatchingResult", matchingResultApi);
        registrationApi.addMapping("/api/matchresult");

    }
}
