package org.progressoft.reconciliation;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.progressoft.reconciliation.Filters.LoginFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.when;

public class LoginCheckerFilterTest {
    @Test
    public void givenInvalidSession_whenDoFilter_thenRedirectToLogin() throws IOException, ServletException {
        LoginFilter filter = new LoginFilter();
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        FilterChain chain = Mockito.mock(FilterChain.class);
        HttpSession session = Mockito.mock(HttpSession.class);

        when(session.getAttribute("username")).thenReturn(null);
        when(session.getAttribute("password")).thenReturn(null);
        when(request.getSession()).thenReturn(session);
        when(request.getContextPath()).thenReturn("/app");
        filter.doFilter(request, response, chain);
        Mockito.verify(response).sendRedirect("/app/index");
    }
}

