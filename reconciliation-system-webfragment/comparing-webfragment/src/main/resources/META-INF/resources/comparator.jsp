<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>

    <meta charset="UTF-8">
    <title>comparator</title>
</head>
<body>
<h3>File Upload:</h3>
Select a file to upload: <br />
<form action = "UploadServlet" method = "post"
      enctype = "multipart/form-data">
    <input type = "file" name = "file" size = "50" />
    <br />
    <input type = "submit" value = "Upload File" />
</form>
<form action="${pageContext.request.contextPath}/comparator" method="POST">
    <table>
        <tr>
            <td>Source Path</td>
            <td><input type="text" aria-colspan="" name="sourcePath"></td>
            <td> Source Extension</td>
            <td><input type="text" name="sourceExtension"></td>
        </tr>
        <tr>
            <td>Target Path</td>
            <td><input type="text" aria-colspan="" name="targetPath"></td>
            <td> Target Extension</td>
            <td><input type="text" name="targetExtension"></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit"/>&nbsp;
                <input type="reset"/>
            </td>
        </tr>
    </table>
    <p>
        <c:if test="${requestScope.result ne null}">
            Result files are available in directory: ${requestScope.result}
        </c:if>
    </p>
</form>
</body>
</html>