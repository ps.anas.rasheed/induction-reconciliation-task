package org.progressoft.reconciliation.helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.progressoft.engine.Record;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class HandleJsonResult {
    static Logger logger = Logger.getLogger(HandleJsonResult.class.getName());

    public static void printJsonResult(HttpServletResponse resp, List<Record> records) {
        try {
            GsonBuilder builder = new GsonBuilder();
            builder.setDateFormat("yyyy-MM-dd");
            Gson gson = builder.create();

            gson.toJson(records, resp.getWriter());
        } catch (IOException exc) {
            // TODO swallowing exception
            logger.warning(exc.getMessage());
        }
    }
}
