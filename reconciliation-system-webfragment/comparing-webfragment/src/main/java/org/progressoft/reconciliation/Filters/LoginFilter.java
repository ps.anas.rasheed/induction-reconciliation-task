package org.progressoft.reconciliation.Filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

public class LoginFilter implements Filter {
    private Logger logger = Logger.getLogger(LoginFilter.class.getName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        handleResult(filterChain, (HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse);

    }

    private void handleResult(FilterChain filterChain, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        // TODO this should only check if I have a session or not
        String username = (String) req.getSession().getAttribute("username");
        String password = (String) req.getSession().getAttribute("password");
        if (!validateInfo(username, password, req))
            resp.sendRedirect(req.getContextPath() + "/index");
        else {
            req.getSession().removeAttribute("loginFail");
            filterChain.doFilter(req, resp);
        }
    }


    @Override
    public void destroy() {

    }

    private boolean validateInfo(String username, String password, HttpServletRequest req) {
        if (username == null || password == null) {
            req.getSession().setAttribute("loginFail", "User name and password are required");
            return false;
        }

        if (username.isEmpty() || password.isEmpty()) {
            req.getSession().setAttribute("loginFail", "User name and password are required");
            return false;
        }

        if (username.equalsIgnoreCase("1") && password.equalsIgnoreCase("1"))
            return true;

        req.getSession().setAttribute("loginFail", "User name or password is wrong");
        return false;

    }
}
