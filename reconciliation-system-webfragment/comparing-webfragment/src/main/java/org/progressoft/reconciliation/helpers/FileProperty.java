package org.progressoft.reconciliation.helpers;

import java.io.File;

public class FileProperty {

    private String sourceName;
    private String sourceExtension;
    private String targetName;
    private String targetExtension;
    private File sourceFile;
    private File targetFile;

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public void setSourceExtension(String sourceExtension) {
        this.sourceExtension = sourceExtension;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public void setTargetExtension(String targetExtension) {
        this.targetExtension = targetExtension;
    }

    public File getSourceFile() {
        return sourceFile;
    }

    public String getTargetExtension() {
        return targetExtension;
    }

    public String getSourceExtension() {
        return sourceExtension;
    }

    public void setSourceFile(File sourceFile) {
        this.sourceFile = sourceFile;
    }

    public void setTargetFile(File targetFile) {
        this.targetFile = targetFile;
    }

    public String getSourceName() {
        return sourceName;
    }

    public String getTargetName() {
        return targetName;
    }

    public File getTargetFile() {
        return targetFile;
    }
}

