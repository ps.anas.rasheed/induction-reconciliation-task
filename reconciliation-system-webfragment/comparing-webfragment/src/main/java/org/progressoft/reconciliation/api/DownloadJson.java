package org.progressoft.reconciliation.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.progressoft.Engine.FinalJsonReader;
import org.progressoft.engine.Record;
import org.progressoft.helpers.AllRecordsConsumer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class DownloadJson extends HttpServlet {
    private Logger logger = Logger.getLogger(DownloadJson.class.getName());
    // TODO this is too much wrong, the servlet is a single object in the container, on concurrent requests this
    // would hold a path for another user
    private Path path;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try {
            preparePath(resp, req);
            Map<String, Record> records = getRecords();
            List<Record> finalRecords = new ArrayList<>(records.values());
            handleResult(finalRecords, resp);
        } catch (Exception e) {
            // TODO propogate the exception as ServletException
            logger.warning(e.getMessage());
        }
    }

    private void handleResult(List<Record> finalRecords, HttpServletResponse resp) throws IOException {
        Gson gson;
        // TODO the json build is repeated in multiple servlets
        GsonBuilder builder = new GsonBuilder();
        builder.setDateFormat("yyyy-MM-dd");
        builder.setPrettyPrinting();
        gson = builder.create();
        gson.toJson(finalRecords, resp.getWriter());
    }

    private List<Record> prepareList(Map<String, Record> records) {
        List<Record> finalRecords = new ArrayList<>();
        records.forEach((s, record) -> {
            finalRecords.add(record);
        });
        return new ArrayList<>(finalRecords);
    }

    private Map<String, Record> getRecords() {
        FinalJsonReader reader = new FinalJsonReader(path);
        AllRecordsConsumer allRecordsConsumer = new AllRecordsConsumer();
        reader.read(allRecordsConsumer);
        return allRecordsConsumer.getRecords();
    }

    private void preparePath(HttpServletResponse resp, HttpServletRequest req) {
        resp.setContentType("text/plain");
        resp.setHeader("Content-disposition", "attachment; filename=" + req.getPathInfo().substring(1));
        // TODO return it
        path = Paths.get(req.getSession().getAttribute("resultPath") + req.getPathInfo());
    }
}
