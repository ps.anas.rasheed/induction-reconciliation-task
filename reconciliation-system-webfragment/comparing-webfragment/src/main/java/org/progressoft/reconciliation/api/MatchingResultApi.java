package org.progressoft.reconciliation.api;

import org.progressoft.engine.Record;
import org.progressoft.reconciliation.helpers.HandleJsonResult;
import org.progressoft.reconciliation.helpers.ParsingFiles;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class MatchingResultApi extends HttpServlet {
    private Logger logger = Logger.getLogger(MatchingResultApi.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // TODO you could enhance this by open an input stream to the file then write it directly to the response
        List<Record> records = ParsingFiles.getRecords(req.getSession().getAttribute("resultPath") + "/matching-transaction.json");
        HandleJsonResult.printJsonResult(resp, records);
    }

}
