package org.progressoft.reconciliation.servlets;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.progressoft.reconciliation.helpers.FileProperty;
import org.progressoft.system.FilesDataSourceFactory;
import org.progressoft.system.PathHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class UploadServlet extends HttpServlet {
    //
    private static final String uploadDir = "./upload";
    // TODO this is not thread safe
    private FileProperty fileProperty;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setErrorAttr(req);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/upload.jsp");
        requestDispatcher.forward(req, resp);
    }

    private void setErrorAttr(HttpServletRequest req) {
        String error = (String) req.getSession().getAttribute("error");
        req.setAttribute("uploadFail", error);
    }

    @Override
    public void init() throws ServletException {
        fileProperty = new FileProperty();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            cleanDirIfExist();
            if (!isMultipart(req, resp))
                return;

            setUpUpload(req, resp);
        } catch (FileUploadException | IOException e) {
            reDirectError(e, req, resp);
        }
    }

    private void setUpUpload(HttpServletRequest req, HttpServletResponse resp) throws IOException, FileUploadException {
        ServletFileUpload upload = getServletFileUpload();
        parseFileRequest(upload.parseRequest(req), req, resp);
        validateFiles();

        req.getSession().setAttribute("fileProperty", fileProperty);
        resp.sendRedirect(req.getContextPath() + "/compare");
    }

    private boolean isMultipart(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        if (!ServletFileUpload.isMultipartContent(req)) {
            req.getSession().setAttribute("error", "Nothing to upload ");
            req.getRequestDispatcher(req.getContextPath() + "/upload").forward(req, resp);
            return false;
        }
        return true;
    }

    private void validateFiles() {
        PathHelper source = new PathHelper(fileProperty.getSourceFile().toPath().toAbsolutePath(), fileProperty.getSourceExtension());
        PathHelper target = new PathHelper(fileProperty.getTargetFile().toPath().toAbsolutePath(), fileProperty.getTargetExtension());
    }

    private ServletFileUpload getServletFileUpload() {
        FileItemFactory itemFactory = new DiskFileItemFactory();
        return new ServletFileUpload(itemFactory);
    }

    private void cleanDirIfExist() throws IOException {
        if (Files.isDirectory(Paths.get(uploadDir)))
            // TODO this clean up will clean files for all users
            FileUtils.cleanDirectory(new File(uploadDir));
    }

    private void parseFileRequest(List<FileItem> items, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        File file = null;
        for (FileItem item : items) {
            if (saveFields(item, item.getFieldName()))
                continue;

            String contentType = item.getContentType();
            contentType = contentTypeToMobileCase(contentType);
            File directory = new File(uploadDir);
            directory.mkdir();
            String filename = item.getName().split("\\.")[0];
            try {
                file = File.createTempFile(filename, suffixFactory(contentType), directory);
            } catch (IOException e) {
                reDirectError(e, req, resp);
            }
            try {
                item.write(file);
            } catch (Exception e) {
                reDirectError(e, req, resp);
            }
            // TODO you should create and return the file property
            if (fileProperty.getSourceFile() == null)
                fileProperty.setSourceFile(file);
            else
                fileProperty.setTargetFile(file);
        }
    }

    private void reDirectError(Exception e, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().setAttribute("error", e.getMessage());
        resp.sendRedirect(req.getContextPath() + "/upload");

    }

    private String contentTypeToMobileCase(String contentType) {
        if (contentType.equalsIgnoreCase("text/comma-separated-values")) {
            contentType = "text/csv";
        }
        return contentType;
    }

    private boolean saveFields(FileItem item, String fieldName) {
        switch (fieldName) {
            case "sourceName":
                fileProperty.setSourceName(item.getString());
                return true;
            case "sourceType":
                fileProperty.setSourceExtension(item.getString());
                return true;
            case "targetName":
                fileProperty.setTargetName(item.getString());
                return true;
            case "targetType":
                fileProperty.setTargetExtension(item.getString());
                return true;
        }
        return false;
    }

    private String suffixFactory(String contentType) {
        emptyValidation(contentType);
        List<String> availableExtensions = FilesDataSourceFactory.getAvailableExtensions();
        for (String availableExtension : availableExtensions) {
            if (contentType.contains(availableExtension))
                return "." + availableExtension;
        }

        throw new IllegalStateException("the type of this file is not support " + contentType + " please fill a correct type");
    }

    private void emptyValidation(String contentType) {
        if (contentType.trim().isEmpty())
            throw new IllegalStateException("the type is empty please fill the type :)");
    }

}
