package org.progressoft.reconciliation.api;

import org.progressoft.engine.Record;
import org.progressoft.reconciliation.helpers.HandleJsonResult;
import org.progressoft.reconciliation.helpers.ParsingFiles;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class MisMatchResultApi extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Record> records = ParsingFiles.getRecords(req.getSession().getAttribute("resultPath") + "/mismatched-transaction.json");
        HandleJsonResult.printJsonResult(resp, records);

    }


}
