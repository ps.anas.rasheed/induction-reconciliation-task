package org.progressoft.reconciliation.Filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UploadFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        handleResult(filterChain, (HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse);
    }

    private void handleResult(FilterChain filterChain, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        if (req.getSession().getAttribute("fileProperty") != null) {
            req.getSession().removeAttribute("error");
            filterChain.doFilter(req, resp);
        } else {
            req.getSession().setAttribute("error", "please fill all Fields");
            resp.sendRedirect(req.getContextPath() + "/upload");
        }
    }

    @Override
    public void destroy() {

    }

}
