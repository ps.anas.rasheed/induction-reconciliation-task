package org.progressoft.reconciliation.api;

import org.progressoft.Engine.FinalCsvReader;
import org.progressoft.engine.Record;
import org.progressoft.helpers.AllRecordsConsumer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.logging.Logger;

public class DownloadCSV extends HttpServlet {
    private Path path;
    private Logger logger;

    public DownloadCSV() {
        logger = Logger.getLogger(DownloadCSV.class.getName());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        preparePath(resp, req);
        handleResult(resp);
    }

    private void handleResult(HttpServletResponse resp) {
        FinalCsvReader reader = new FinalCsvReader(path);
        Map<String, Record> records = getRecords(reader);
        printRecords(records, resp);
    }



    private void printRecords(Map<String, Record> records, HttpServletResponse resp) {
        records.forEach((s, record) -> {
            try {
                resp.getWriter().append(record.getFoundIn() != null ? record.getFoundIn() : "").append(",").append(record.getTransId()).append(",").append(record.getAmount() + "").append(",").append(record.getCurrencyCode()).append(",").append(record.getDate().toString()).append("\n");
            } catch (IOException e) {
                logger.warning(e.getMessage());
            }

        });
    }

    private Map<String, Record> getRecords(FinalCsvReader reader) {
        AllRecordsConsumer allRecordsConsumer = new AllRecordsConsumer();
        reader.read(allRecordsConsumer);
        return allRecordsConsumer.getRecords();
    }

    private void preparePath(HttpServletResponse resp, HttpServletRequest req) {
        resp.setContentType("text/plain");
        resp.setHeader("Content-disposition", "attachment; filename=" + req.getPathInfo().substring(1));
        path = Paths.get((String) req.getSession().getAttribute("resultPath") + req.getPathInfo());
    }
}

