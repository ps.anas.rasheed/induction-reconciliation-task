package org.progressoft.reconciliation.Filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResultFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        handleResult(filterChain, (HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse);
    }

    private void handleResult(FilterChain filterChain, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String path = (String) req.getSession().getAttribute("resultPath");
        if (path == null)
            resp.sendRedirect(req.getContextPath() + "/compare");
        else
            filterChain.doFilter(req, resp);
    }

    @Override
    public void destroy() {

    }
}
