package org.progressoft.reconciliation.helpers;

import org.progressoft.engine.Record;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ParsingFiles {

    public static List<Record> getRecords(String resultPath) throws FileNotFoundException {
        JsonArray allRecords = readJsonFile(resultPath);
        List<Record> records = parseToJson(resultPath, allRecords);
        return records;
    }

    private static List<Record> parseToJson(String resultPath, JsonArray allRecords) {
        List<Record> records = new ArrayList<>();
        for (int i = 0; i < allRecords.size(); i++) {
            JsonObject jsonObject = allRecords.getJsonObject(i);
            records.add(handleResult(resultPath, jsonObject));
        }
        return records;
    }

    private static Record handleResult(String resultPath, JsonObject jsonObject) {
        String transId = jsonObject.getString("transId");
        int amount = jsonObject.getInt("amount");
        String currencyCode = jsonObject.getString("currencyCode");
        Date date = new Date(jsonObject.getString("date"));
        if (resultPath.contains("matching-transaction.json"))
            return new Record(transId, BigDecimal.valueOf(amount), currencyCode, date);
        else
            return new Record(transId, BigDecimal.valueOf(amount), currencyCode, date, jsonObject.getString("foundIn"));
    }

    private static JsonArray readJsonFile(String resultPath) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream(new File(resultPath));
        JsonReader jsonReader = Json.createReader(inputStream);
        return jsonReader.readArray();
    }
}
