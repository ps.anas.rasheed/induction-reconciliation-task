package org.progressoft.reconciliation.servlets;

import org.progressoft.reconciliation.helpers.FileProperty;
import org.progressoft.system.FilesDataSourceFactory;
import org.progressoft.system.PathHelper;
import org.progressoft.utils.FileComparing;
import org.progressoft.utils.FilesDataSource;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ServiceLoader;

public class ComparatorServlet extends HttpServlet {

    public ComparatorServlet() {
        System.out.println("entered servlet");
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/compare.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setUpCompare(req, resp);
    }

    private void setUpCompare(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        FileProperty fileProperty = getFileProperty(req);
        try {

            Path sourcePath = fileProperty.getSourceFile().toPath().toAbsolutePath();
            String sourceType = fileProperty.getSourceExtension();
            Path targetPath = fileProperty.getTargetFile().toPath().toAbsolutePath();
            String targetType = fileProperty.getTargetExtension();

            PathHelper sourceObj = new PathHelper(sourcePath, sourceType);
            PathHelper targetObj = new PathHelper(targetPath, targetType);

            doCompare(sourceObj, targetObj, req);
        } catch (Exception exc) {
            req.getSession().setAttribute("error", "error while comparing " + exc.getMessage());
            resp.sendRedirect(req.getContextPath() + "/upload");
            return;
        }
        resp.sendRedirect(req.getContextPath() + "/result");
    }

    private void doCompare(PathHelper sourceObj, PathHelper targetObj, HttpServletRequest req) {
        ServiceLoader<FileComparing> serviceLoader = ServiceLoader.load(FileComparing.class);
        for (FileComparing fileComparing : serviceLoader) {
            System.out.println("");
            String compare = fileComparing.compare(setupDataSource(sourceObj), setupDataSource(targetObj));
            req.getSession().setAttribute("resultPath", compare.substring(0, compare.length() - 2));
        }
    }

    private FileProperty getFileProperty(HttpServletRequest req) {
        HttpSession session = req.getSession();
        return (FileProperty) session.getAttribute("fileProperty");
    }

    private static FilesDataSource setupDataSource(PathHelper path) {
        return FilesDataSourceFactory.getSourceExtension(path);
    }

}
