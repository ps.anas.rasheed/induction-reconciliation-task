package org.progressoft.reconciliation.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.progressoft.system.FilesDataSourceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ExtensionApi extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> availableExtensions = FilesDataSourceFactory.getAvailableExtensions();
        printResult(resp, availableExtensions);
    }

    private void printResult(HttpServletResponse resp, List<String> availableExtensions) throws IOException {
        Gson gson= new GsonBuilder().create();
        resp.setContentType("application/json");
        // TODO enhance: always return a json object {extensions:[]}
        gson.toJson(availableExtensions,resp.getWriter());
    }
}
