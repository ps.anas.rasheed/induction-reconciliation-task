package org.progressoft.reconciliation.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Logger;

public class IndexServlet extends HttpServlet {
    private Logger logger = Logger.getLogger(IndexServlet.class.getName());

    public IndexServlet() {
        logger.warning("entered " + IndexServlet.class.getName());
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/index.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        setSessionAttribute(req);
        resp.sendRedirect(req.getContextPath() + "/upload");
    }

    private void setSessionAttribute(HttpServletRequest req) {
        HttpSession session = req.getSession();
        // TODO keep the checking also in here
        session.setAttribute("clicked", "true");
        session.setAttribute("username", req.getParameter("username"));
        session.setAttribute("password", req.getParameter("password"));
        session.setMaxInactiveInterval(60 * 30);

    }
}
