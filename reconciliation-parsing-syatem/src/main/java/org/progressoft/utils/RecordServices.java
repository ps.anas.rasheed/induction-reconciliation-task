package org.progressoft.utils;

import org.progressoft.engine.Record;

import java.util.Map;

public interface RecordServices {
    Record findRecord(String transId);

    Map<String, Record> getRemainingRecords();

    boolean contains(String transId);

    Record ignoreRecord(String transId);
}
