package org.progressoft.utils;

public interface Reader {
    void read(Consumer consumer);
}
