package org.progressoft.utils;

import org.progressoft.engine.Record;

public interface Consumer {
    boolean isEnd();

    void accept(Record record);
}
