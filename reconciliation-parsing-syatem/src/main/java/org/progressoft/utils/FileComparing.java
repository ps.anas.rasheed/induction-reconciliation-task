package org.progressoft.utils;

public interface FileComparing {

    String compare(FilesDataSource sourceRecordServices,FilesDataSource targetRecordServices);
}
