package org.progressoft.engine;

import org.progressoft.utils.FileComparing;
import org.progressoft.utils.FilesDataSource;
import org.progressoft.utils.RecordServices;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SimpleFilesComparing implements FileComparing {

    private RecordServices sourceRecordServices;
    private RecordServices targetRecordServices;
    private static final String MATCH_HEADER = "transaction id,amount,currecny code,value date\n";
    private static final String UNMATCH_HEADER = "found in file,transaction id,amount,currecny code,value date\n";

    @Override
    public String compare(FilesDataSource source, FilesDataSource target) {
        validateDataSource(source, "source");
        validateDataSource(target, "target");
        sourceRecordServices = source.getServices();
        targetRecordServices = target.getServices();
        return doCompareToGetPath();
    }

    private String doCompareToGetPath() {
        CompareResult result = processRecords();
        String path = returnResultPath(result);
        System.out.println("Reconciliation finished");
        System.out.println("Result files are available in directory " + path);
        return path;
    }

    private class CompareResult {

        List<Record> matchingTrans = new ArrayList<>();
        List<Record> missingTrans = new ArrayList<>();
        List<Record> misMatchedTrans = new ArrayList<>();

        public List<Record> getMatchingTrans() {
            return new ArrayList(matchingTrans);
        }

        public List<Record> getMissingTrans() {
            return new ArrayList(missingTrans);
        }

        public List<Record> getMisMatchedTrans() {
            return new ArrayList(misMatchedTrans);
        }

        public void addMatchingTrans(Record record) {
            matchingTrans.add(record);
        }

        public void addMissingTrans(Record record) {
            missingTrans.add(record);
        }

        public void addMisMatchedTrans(Record record) {
            misMatchedTrans.add(record);
        }
    }

    private CompareResult processRecords() {
        CompareResult result = new CompareResult();

        Map<String, Record> sourceRecords = getSourceRecords();
        for (Map.Entry<String, Record> entry : sourceRecords.entrySet()) {
            compareTransaction(result, entry);
        }
        getRemainingTargetRecords(result);
        return result;
    }

    private void compareTransaction(CompareResult result, Map.Entry<String, Record> entry) {
        Record sourceRecord = entry.getValue();
        String transId = sourceRecord.getTransId();
        if (!targetRecordServices.contains(transId)) {
            sourceRecord.setFoundIn("SOURCE");
            result.addMissingTrans(sourceRecord);
            return;
        }
        Record targetRecord = targetRecordServices.ignoreRecord(transId);
        if (matches(sourceRecord, targetRecord)) {
            result.addMatchingTrans(sourceRecord);
            return;
        }
        addMisMatchedRecords(result, sourceRecord, targetRecord);
    }

    private void addMisMatchedRecords(CompareResult types, Record sourceRecord, Record targetRecord) {
        sourceRecord.setFoundIn("SOURCE");
        types.addMisMatchedTrans(sourceRecord);
        targetRecord.setFoundIn("TARGET");
        types.addMisMatchedTrans(targetRecord);
    }

    private void getRemainingTargetRecords(CompareResult types) {
        Map<String, Record> targetRecords = targetRecordServices.getRemainingRecords();
        targetRecords.forEach((s, record) -> {
            record.setFoundIn("TARGET");
            types.addMissingTrans(record);
            targetRecords.remove(record);
        });
    }

    private String returnResultPath(CompareResult types) {
        File csvFiles = addCsvFiles(types);
        File jsonFiles = addJsonFiles(types);
        String finalPath = csvFiles.getParentFile().getAbsolutePath();
        if (Objects.isNull(finalPath))
            throw new IllegalStateException("final path of url is Empty !! ");
        return finalPath;
    }

    private File addJsonFiles(CompareResult types) {
        WriteInFiles.printFiles(types.getMatchingTrans(), MATCH_HEADER, "matching-transaction.json");
        WriteInFiles.printFiles(types.getMisMatchedTrans(), UNMATCH_HEADER, "mismatched-transaction.json");
        return WriteInFiles.printFiles(types.getMissingTrans(), UNMATCH_HEADER, "missing-transaction.json");
    }

    private File addCsvFiles(CompareResult types) {
        WriteInFiles.printFiles(types.getMatchingTrans(), MATCH_HEADER, "matching-transaction.csv");
        WriteInFiles.printFiles(types.getMisMatchedTrans(), UNMATCH_HEADER, "mismatched-transaction.csv");
        return WriteInFiles.printFiles(types.getMissingTrans(), UNMATCH_HEADER, "missing-transaction.csv");
    }

    // TODO have this method in another class DONE
    private Map<String, Record> getSourceRecords() {
        return sourceRecordServices.getRemainingRecords();
    }

    private boolean matches(Record source, Record target) {
        return source.equals(target); // I overrided .equals in Record class
    }

    private void validateDataSource(FilesDataSource dataSource, String type) {
        if (Objects.isNull(dataSource))
            throw new NullPointerException("the" + type + " is null");
        if (Objects.isNull(dataSource.getServices()))
            throw new NullPointerException("the" + type + " services is null");
    }
}
