package org.progressoft.engine;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.progressoft.exceptions.FileExcptions;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class WriteInFiles {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static File printFiles(List<Record> list, String header, String fileName) {
        File file = new File(".");
        File outputFile = new File(file, fileName);
        try (OutputStream os = new FileOutputStream(outputFile); OutputStreamWriter outputStreamWriter = new OutputStreamWriter(os);) {

            if (fileName.substring(fileName.lastIndexOf(".") + 1).equalsIgnoreCase("csv")) {
                addHeader(header, os);
                handleCsvRecords(list, fileName, os);
            } else
                handleJsonRecords(list, outputStreamWriter);
        } catch (IOException exc) {
            throw new FileExcptions("can not read the file ", exc);
        }
        return outputFile;
    }

    private static void handleCsvRecords(List<Record> list, String fileName, OutputStream os) throws IOException {
        for (Record record : list) {
            addCSVOutFiles(os, record);
        }
        os.flush();
    }

    private static void handleJsonRecords(List<Record> list, OutputStreamWriter os) throws IOException {
        os.write("[" + "\n");
        int counter = 0;
        for (Record record : list) {
            addJSONOutFiles(record, os);
            if (++counter != list.size())
                os.write(",");
            os.write("\n");
        }
        os.write("\n");
        os.write("]");
        os.flush();
    }

    private static void addJSONOutFiles(Record record, OutputStreamWriter os) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        gson.toJson(record, os);
    }

    private static void addCSVOutFiles(OutputStream os, Record record) {
        String comma = ",";
        if (!Objects.isNull(record.getFoundIn()) && !record.getFoundIn().isEmpty())
            writeInFile(os, record.getFoundIn() + comma);

        writeInFile(os, record.getTransId() + comma);
        writeInFile(os, (record.getAmount()) + comma);
        writeInFile(os, record.getCurrencyCode() + comma);
        writeInFile(os, dateFormat.format(record.getDate()));
        addNewLine(os, "\n");
    }

    private static void addNewLine(OutputStream os, String s) {
        writeInFile(os, s);
    }

    private static void addHeader(String header, OutputStream os) {
        writeInFile(os, header);
    }

    private static void writeInFile(OutputStream os, String attr) {
        byte[] bytes;
        bytes = attr.getBytes();
        try {
            os.write(bytes);
        } catch (IOException e) {
            throw new FileExcptions("can not write to file ", e);
        }
    }
}
