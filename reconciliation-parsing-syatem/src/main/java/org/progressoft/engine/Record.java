package org.progressoft.engine;

import org.progressoft.exceptions.ParsingException;

import java.math.BigDecimal;
import java.util.Date;

public class Record {

    private String transId;
    private BigDecimal amount;
    private String currencyCode;
    private Date date;
    private String foundIn;

    public String getFoundIn() {
        return foundIn;
    }

    public void setFoundIn(String foundIn) {
        this.foundIn = foundIn;
    }

    public Record(String transId, BigDecimal amount, String currencyCode, Date date, String foundIn) {
        this.transId = transId;
        this.amount = amount;
        this.currencyCode = currencyCode;
        this.date = date;
        this.foundIn = foundIn;
    }

    public Record(String transId, BigDecimal amount, String currencyCode, Date date) {
        this.transId = transId.trim();
        this.amount = amount;
        this.currencyCode = currencyCode.trim();
        this.date = date;
    }


    public String getTransId() {
        return transId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Date getDate() {
        return date;
    }



    public String[] toArray() {
        return new String[]{getTransId(), getAmount().setScale(1).toString(), getCurrencyCode(), getDate().toString()};
    }

    @Override
    public boolean equals(Object object) {
     if(object instanceof Record)
       //  System.out.println("yes");
        if (this == object)
            return true;
        if (object == null || getClass() != object.getClass())
            return false;
        Record target = (Record) object;
        try {
            if (!this.transId.equalsIgnoreCase(target.transId) ||
                    !this.currencyCode.equalsIgnoreCase(target.currencyCode) ||
                    !this.date.equals(target.date) ||
                    this.amount.compareTo(target.amount) != 0
            )
                return false;
        } catch (Exception e) {
            throw new ParsingException("unable to parse ", e);
        }
        return true;
    }
}
