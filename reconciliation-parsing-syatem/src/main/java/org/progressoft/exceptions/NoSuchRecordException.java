package org.progressoft.exceptions;

public class NoSuchRecordException extends RuntimeException {
    public NoSuchRecordException(String msg){
        super(msg);
    }
}
