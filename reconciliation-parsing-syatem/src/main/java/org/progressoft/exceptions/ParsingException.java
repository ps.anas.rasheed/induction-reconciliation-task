package org.progressoft.exceptions;

public class ParsingException extends RuntimeException {

    public ParsingException(String msg, Exception e) {
        super(msg, e);
    }
}
