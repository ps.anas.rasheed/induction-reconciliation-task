package org.progressoft.exceptions;

public class FileExcptions extends RuntimeException {

    public FileExcptions(String msg) {
        super(msg);
    }

    public FileExcptions(String msg, Exception e) {
        super(msg, e);
    }
}
